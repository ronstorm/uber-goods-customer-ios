//
//  LoginViewModel.swift
//  UG Customer
//
//  Created by Amit Sen on 3/8/18.
//  Copyright © 2018 Amit Sen. All rights reserved.
//

import ASNet
import Bond

class LoginViewModel: BaseViewModel {
    let email = Observable<String?>("")
    let password = Observable<String?>("")
    
    private struct ParamKey {
        let username = "username"
        let password = "password"
        let grant_type = "grant_type"
        let client_id = "client_id"
        let client_secret = "client_secret"
    }

    override init() {
        super.init()
    }
    
    func doLogin(onComplete callback: @escaping(Token) -> Void) {
        
        let key = ParamKey.init()
        
        let params: Parameters? = [
            key.username: email.value!,
            key.password: password.value!,
            key.grant_type: "password",
            key.client_id: dataStoreManager.getClientId(),
            key.client_secret: dataStoreManager.getClientSecret()
        ]
        
        let result = App.sharedInstance.dataStoreManager.getClientId() + ":" + App.sharedInstance.dataStoreManager.getClientSecret()
        
        let header: HTTPHeader = [
            "Authorization": "Basic " + result.base64Encoded()!,
            "Content-Type": "application/x-www-form-urlencoded",
            "Cache-Control": "no-cache",
            "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
        ]
        
        networkManager.apiCallForObjectResponse(endpointURL: apiPaths.tokenUrl,
                                                httpMethod: .post,
                                                httpHeader: header,
                                                parameters: params,
                                                isMultiPart: false,
                                                filesWhenMultipart: nil,
                                                returningType: Token.self) { (token, jsonData) in
                                                    
                                                    self.dataStoreManager.setToken(token: token)
                                                    callback(token)
        }
    }
}
