//
//  BaseViewModel.swift
//  Welltravel
//
//  Created by Amit Sen on 11/20/17.
//  Copyright © 2017 Welldev.io. All rights reserved.
//

import Foundation

class BaseViewModel {

    var texts: Constants.Texts!
    var colors: Constants.Color!
    var regex: Constants.Regex!
    var timeFormats: Constants.TimeFormats!
    var fonts: Constants.Fonts!
    var apiPaths: Constants.APIPaths!
    var errorManager: ErrorManager!
    var alert: Alert!
    var userDefaultsManager: UserDefaultsManager!
    var validityManager: ValidityManager!
    var uiHelper: UIHelper!
    var dataStoreManager: DataStoreManager!
    var vivid: Vivid!
    var dateTimeHelper: DateTimeHelper!
    var mockDataManager: MockDataManager!
    var routes: Routes!
    var networkManager: NetworkManager!
    var configKeys: Constants.ConfigKeys!
    var images: Constants.Images!
    var fileNames: Constants.FileNames!
    var loader: Loader!
    
    
    init() {
        let app = App.sharedInstance
        
        texts = app.texts
        colors = app.colors
        regex = app.regex
        timeFormats = app.timeFormats
        fonts = app.fonts
        apiPaths = app.apiPaths
        errorManager = app.errorManager
        alert = app.alert
        userDefaultsManager = app.userDefaultsManager
        validityManager = app.validityManager
        uiHelper = app.uiHelper
        dataStoreManager = app.dataStoreManager
        vivid = app.vivid
        dateTimeHelper = app.dateTimeHelper
        mockDataManager = app.mockDataManager
        routes = app.routes
        networkManager = app.networkManager
        print("nm assigned in BaseViewModel")
        configKeys = app.configKeys
        images = app.images
        fileNames = app.fileNames
        loader = app.loader
    }
}
