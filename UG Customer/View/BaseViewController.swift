//
//  BaseViewController.swift
//  Welltravel
//
//  Created by Amit Sen on 11/21/17.
//  Copyright © 2017 Welldev.io. All rights reserved.
//

import UIKit

enum CornerButton {
    case menu
    case home
    case back
}

class BaseViewController: UIViewController, SlideMenuDelegate {
    // Private variables
    private var navBar: CustomNavigationBar!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // private methods
    private func extraTop() -> CGFloat {
        var top: CGFloat = 0
        if #available(iOS 11.0, *) {
            if let t = UIApplication.shared.keyWindow?.safeAreaInsets.top {
                if t > 0 {
                    top = t - 20.0
                } else {
                    top = t
                }
            }
        }
        return top
    }
    
    private func onSlideMenuButtonPressed(_ sender: UIButton) {
        if sender.tag == 10 {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1)
            
            sender.tag = 0
            
            let viewMenuBack: UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (_) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        // swiftlint:disable line_length
        let menuVC = UIStoryboard(name: "Menu", 
                                  bundle: nil).instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
        // swiftlint:enable line_length
        
        menuVC.menuBtn = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame = CGRect(x: 0 - UIScreen.main.bounds.size.width, 
                                 y: 0, 
                                 width: UIScreen.main.bounds.size.width, 
                                 height: UIScreen.main.bounds.size.height)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame = CGRect(x: 0, 
                                     y: 0, 
                                     width: UIScreen.main.bounds.size.width, 
                                     height: UIScreen.main.bounds.size.height)
            sender.isEnabled = true
        }, completion: nil)
    }
    
    private func openViewControllerBasedOnIdentifier(_ strIdentifier: String, storyboard: UIStoryboard) {
        let destViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController: UIViewController = self.navigationController!.topViewController!
        
        if topViewController.restorationIdentifier! == destViewController.restorationIdentifier! {
            print("Same View Controller")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }

    // public methods
    open func placeCornerBtn(withCornerBtn cornerBtn: CornerButton) {
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(x: 20, y: 20 + extraTop(), width: 29, height: 19)
        
        switch cornerBtn {
        case .menu:
            btn.setBackgroundImage(UIImage(named: "menu"), for: .normal)
            _ = btn.reactive.tap.observeNext {
                self.onSlideMenuButtonPressed(btn)
            }
        case .home:
            btn.setBackgroundImage(UIImage(named: "home"), for: .normal)
            
        case .back:
            btn.setBackgroundImage(UIImage(named: "back"), for: .normal)
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    open func placeNavBar(withTitle title: String, isMenuBtnVisible visible: Bool) {
        navBar = CustomNavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 75.0 + extraTop()))
        navBar.activateBtn(navBarTitle: title,
                           isMenuBtnVisible: visible) { btn in
            if visible {
                self.onSlideMenuButtonPressed(btn)
            } else {
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
        self.view.addSubview(navBar)
    }
    
    // Delegates
    // SlideMenuDelegate
    func slideMenuItemSelectedAtIndex(_ index: Int) {
        let topViewController: UIViewController = self.navigationController!.topViewController!
        print("View Controller is : \(topViewController) \n", terminator: "")
        switch index {
        case 0:
            print("EditProfileViewController\n", terminator: "")
//            self.openViewControllerBasedOnIdentifier("EditProfileViewController", 
//                                                     storyboard: UIStoryboard(name: App.sharedInstance.routes.profile,
//                                                                              bundle: nil))
            
        default:
            print("default\n", terminator: "")
        }
    }
}
