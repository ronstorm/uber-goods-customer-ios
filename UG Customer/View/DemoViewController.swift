//
//  DemoViewController.swift
//  Welltravel
//
//  Created by Amit Sen on 11/21/17.
//  Copyright © 2017 Welldev.io. All rights reserved.
//

import UIKit

class DemoViewController: BaseViewController {
    
    // IBOutlets
    
    // public variables

    // private variables

    //Overridden Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // IBActions

    // public methods

    // private methods

    // delegates
}
